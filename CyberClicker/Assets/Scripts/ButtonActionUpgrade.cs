﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonActionUpgrade : MonoBehaviour
{
    public Manager manager;

    public BaseUpgradeData data;


    [SerializeField]
    private float Costincrease = 1.1f;
    [SerializeField]
    private float Researchincrease = 2;


    //Base Action Upgrade
    public float Upgradecost;
    public float UpgradeNumber = 1f;// 1 is required for Code to work// unnessesary
    public Text UpgradeUI;
    public Text UpgradeUIcost;
    public float UpgradeResearchThreshold;
    public Text UpgradeResearchUI;
    public float Upgradeamount = 1f;// nessesary for start


    public void ActionUpgradepress()
    {
        if (manager.counter >= Upgradecost)
        {
            if (manager.Researchcounter >= UpgradeResearchThreshold)
            {
                manager.counter -= Upgradecost;
                manager.Increase += Upgradeamount;
                UpgradeNumber += 1f;
                Upgradecost *= Costincrease;
                UpgradeResearchThreshold *= Researchincrease;
            }

        }
    }

    public void Start()
    {
        Upgradecost = data.Upgradecost;
        UpgradeResearchThreshold = data.UpgradeResearchThreshold;
    }

    public void Update()
    {
        //ActionUpgrade
        UpgradeUI.text = UpgradeNumber.ToString();
        UpgradeUIcost.text = Upgradecost.ToString();
        UpgradeResearchUI.text = UpgradeResearchThreshold.ToString();

    }
}

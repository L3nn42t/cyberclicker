﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PopData", menuName = "PopData")]
public class PopData : ScriptableObject
{
    public int Researchyield;

    public float Factorycost;
}

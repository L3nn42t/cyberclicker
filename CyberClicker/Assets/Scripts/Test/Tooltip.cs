﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{

    public Text tooltiptext;
    [SerializeField]
    public string tooltipmessage;
    // Start is called before the first frame update
    void Start()
    {
        tooltiptext = GameObject.FindGameObjectWithTag("tip").GetComponent<Text>();
        tooltiptext.enabled = false;
    }

    public void OnMouseOver()
    {
        tooltiptext.enabled = true;
        tooltiptext.GetComponent<Text>().text = tooltipmessage;
    }
    public void OnMouseExit()
    {
        tooltiptext.enabled = false;
    }

}

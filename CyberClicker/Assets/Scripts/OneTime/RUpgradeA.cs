﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RUpgradeA : MonoBehaviour
{
    [SerializeField]
    private FactoryResearch Rfactory;


    [SerializeField]
    private float cost = 10f;

    [SerializeField]
    private GameObject AlternativeBranch;
    [SerializeField]
    private GameObject Image;
    [SerializeField]
    private float plusamount = 0.04f;

    [SerializeField]
    private Manager manager;

    [SerializeField]
    private GameObject outline;
    public float imageY;
    [SerializeField]
    private GameObject ImageB;


    public void Opress()
    {
        if (manager.counter >= cost)
        {
            Rfactory.Researchyield += plusamount;
            outline.SetActive(true);
            outline.SetActive(true);
            ImageB.SetActive(true);
            AlternativeBranch.SetActive(false);

            Destroy(this.gameObject);
            Image.transform.position = new Vector3(Image.transform.position.x, Image.transform.position.y - imageY, Image.transform.position.z);
        }


    }
}

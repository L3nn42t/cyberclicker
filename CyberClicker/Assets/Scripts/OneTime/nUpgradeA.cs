﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nUpgradeA : MonoBehaviour
{

    [SerializeField]
    private ButtonFactory factory;
    [SerializeField]
    private GameObject AlternativeBranch;
    [SerializeField]
    private GameObject self;
    [SerializeField]
    private float cost = 10f;

    [SerializeField]
    private GameObject Image;
    [SerializeField]
    private float plusamount = 0.05f;

    [SerializeField]
    private Manager manager;

    [SerializeField]
    private GameObject outline;
    public float imageY;
    [SerializeField]
    private GameObject ImageB;

    public void Opress()
    {
        if (manager.counter >= cost)
        {
            factory.Factoryyield += plusamount;
            outline.SetActive(true);
            ImageB.SetActive(true);
            AlternativeBranch.SetActive(false);

            Destroy(self);
            Image.transform.position = new Vector3(Image.transform.position.x, Image.transform.position.y - imageY, Image.transform.position.z);
        }


    }

}

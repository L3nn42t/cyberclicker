﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FactoryResearch : MonoBehaviour
{
    public Manager manager;
    [SerializeField]
    private float Costincrease = 1.1f;

    //scriptable objects
    public FactoryResearchData Data;
    public FactoryResearchUpgradeData udata;


    //own Variables

    public float Researchyield;
    public float Factorycost;

    public float factorycounter = 0f;
    //UI
    public Text FactoryResearchUI;
    public Text FactoryUIResearchcost;

    //upgrades
    public float FactoryUpgradecost;
    public float FactoryUpgradeNumber = 1f;// is nessecary for increase to work, otherwise 0
    public float FactoryUpgradeResearchThreshold;
    public float Researchincrease;

    public Text FactoryUpgradeUI;
    public Text FactoryUpgradeUIcost;
    public Text FactoryUpgradeResearchUIText;


    // At start, the variables are set equal to their opposites in the SO  
    public void Start()
    {
        //
        Researchyield = Data.Researchyield;
        Factorycost = Data.Factorycost;

        //upgrades
        FactoryUpgradecost = udata.FactoryUpgradecost;
        FactoryUpgradeResearchThreshold = udata.Threshold;
        Researchincrease = udata.Increase;

    }

    public void Increasepress()
    {
        if (manager.counter >= Factorycost)
        {
            manager.counter -= Factorycost;
            factorycounter += 1f;
            Factorycost *= Costincrease;

        }
    }
    public void Upgradepress()
    {
        if (manager.counter >= FactoryUpgradecost)
        {
            if (manager.Researchcounter >= FactoryUpgradeResearchThreshold)
            {
                manager.counter -= FactoryUpgradecost;
                FactoryUpgradeNumber += 1f;
                FactoryUpgradecost *= Costincrease;
                FactoryUpgradeResearchThreshold *= Researchincrease;
            }
        }
    }


    public void Update()
    {
        manager.Researchcounter += Researchyield* factorycounter * Time.deltaTime;

        // UI
        FactoryResearchUI.text = factorycounter.ToString();
        FactoryUIResearchcost.text = Factorycost.ToString();

        //UpgradeUI
        FactoryUpgradeUI.text = FactoryUpgradeNumber.ToString();
        FactoryUpgradeUIcost.text = FactoryUpgradecost.ToString();
        FactoryUpgradeResearchUIText.text = FactoryUpgradeResearchThreshold.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTransition : MonoBehaviour
{
    [SerializeField]
    private GameObject MainGame;
    [SerializeField]
    private GameObject UI;

    [SerializeField]
    private GameObject Upgrades;

    public bool GameISPaused = false;


    public void Transitionpress()
    {
        if(GameISPaused == false)
        {
            UpgradeOn();
        }
        

        if(GameISPaused == true)
        {
            UpgradeOff();
        }
    }


    public void UpgradeOn()
    {
        MainGame.SetActive(false);
        UI.SetActive(false);
        Upgrades.SetActive(true);
        Time.timeScale = 0f;
        GameISPaused = true;
    }

     public void UpgradeOff()
    {
        MainGame.SetActive(true);
        UI.SetActive(true);
        Upgrades.SetActive(false);
        Time.timeScale = 1f;
        GameISPaused = false;

    }

}

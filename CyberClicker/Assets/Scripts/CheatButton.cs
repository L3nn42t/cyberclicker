﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatButton : MonoBehaviour
{

    public Manager manager;

    [SerializeField]
    private float Debugmoney = 100000f;
    [SerializeField]
    private float Debugresearch = 100000f;
    [SerializeField]
    private float Debugpop = 100000f;

    public void Debugpress()
    {
        manager.counter += Debugmoney;
        manager.Researchcounter += Debugresearch;
        manager.Popcounter += Debugpop;
    }
}

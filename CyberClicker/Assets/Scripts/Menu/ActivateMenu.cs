﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMenu : MonoBehaviour
{
    public bool GameISPaused = true;
    public GameObject pauseMenuUI;
    public GameObject MainGame;


    private void Update()
    {


        if (Input.GetKeyDown(KeyCode.Escape))
        {

            if (GameISPaused)
            {
             //   Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        MainGame.SetActive(true);
        Time.timeScale = 1f;
        GameISPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        MainGame.SetActive(false);
        Time.timeScale = 0f;
        GameISPaused = true;

    }
}

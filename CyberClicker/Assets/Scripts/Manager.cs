﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Manager : MonoBehaviour
{
    // Currency Counter, used to buy stuff
    public float counter;
    public Text uicounter;

    public float Increase = 1;

    // ResearchCounter, used to unlock stuff, use if variables
    public float Researchcounter;
    public Text ResearchUIcounter;
    public float Researchincrease = 1;

    // Pop counter
    public float Popcounter;
    public Text PopUIcounter;

    public float Popincrease;
   

   
    

    void Update()
    {
        uicounter.text = counter.ToString();

        ResearchUIcounter.text = Researchcounter.ToString();

        PopUIcounter.text = Popcounter.ToString();
    }

    
}

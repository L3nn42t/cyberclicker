﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FactoryPop : MonoBehaviour
{

    public Manager manager;
    [SerializeField]
    private float Costincrease = 1.1f;

    //scriptable objects
    public FactoryPopData Data;
    public FactoryPopUpgradeData udata;


    //own Variables

    public float Popyield;
    public float FactoryPopcost;

    public float factoryPopcounter = 0f;
    //UI
    public Text FactoryPopUI;
    public Text FactoryUIPopcost;

    //upgrades
    public float FactoryPopUpgradecost;
    public float FactoryPopUpgradeNumber = 1f;// is nessecary for increase to work, otherwise 0
    public float FactoryPopUpgradeResearchThreshold;
    public float Researchincrease;

    public Text FactoryPopUpgradeUI;
    public Text FactoryPopUpgradeUIcost;
    public Text FactoryPopUpgradeResearchUIText;


    // At start, the variables are set equal to their opposites in the SO  Pop
    public void Start()
    {
        //
        Popyield = Data.Popyield;
        FactoryPopcost = Data.Popcost;

        //upgrades
        FactoryPopUpgradecost = udata.PopUpgradecost;
        FactoryPopUpgradeResearchThreshold = udata.PopThreshold;
        Researchincrease = udata.PopIncrease;

    }

    public void Increasepress()
    {
        if (manager.counter >= FactoryPopcost)
        {
            manager.counter -= FactoryPopcost;
            factoryPopcounter += 1f;
            FactoryPopcost *= Costincrease;

        }
    }
    public void Upgradepress()
    {
        if (manager.counter >= FactoryPopUpgradecost)
        {
            if (manager.Researchcounter >= FactoryPopUpgradeResearchThreshold)
            {
                manager.counter -= FactoryPopUpgradecost;
                FactoryPopUpgradeNumber += 1f;
                FactoryPopUpgradecost *= Costincrease;
                FactoryPopUpgradeResearchThreshold *= Researchincrease;
            }
        }
    }


    public void Update()
    {
        manager.Popcounter += Popyield * factoryPopcounter * Time.deltaTime;

        // UI
        FactoryPopUI.text = factoryPopcounter.ToString();
        FactoryUIPopcost.text = FactoryPopcost.ToString();

        //UpgradeUI
        FactoryPopUpgradeUI.text = FactoryPopUpgradeNumber.ToString();
        FactoryPopUpgradeUIcost.text = FactoryPopUpgradecost.ToString();
        FactoryPopUpgradeResearchUIText.text = FactoryPopUpgradeResearchThreshold.ToString();
    }
}

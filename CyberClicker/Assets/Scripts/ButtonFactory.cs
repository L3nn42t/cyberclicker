﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFactory : MonoBehaviour
{
    public Manager manager;
    [SerializeField]
    private float Costincrease = 1.1f;
    //Wip
    public FactoryData data;
    public FactoryUpgradeData udata;

    public Text FactoryUI;
    public Text FactoryUIcost;


    //Factory
    public float factorycounter = 0f;

    public float FactoryCost;
    public float Factoryyield; 
    public float FactoryIncrease;


    //Upgrade
    [SerializeField]
    private float UpgradeCostincrease = 1.1f;
    [SerializeField]
    private float Researchincrease = 2;

    public float FactoryUpgradecost;
    public float FactoryUpgradeNumber = 1f;// is nessecary for increase to work, otherwise 0
    public float FactoryUpgradeResearchThreshold;

    public Text FactoryUpgradeUI;
    public Text FactoryUpgradeUIcost;
    public Text FactoryUpgradeResearchUIText;


    public void Factorypress()
    {
        if (manager.counter >= FactoryCost)
        {
            manager.counter -= FactoryCost;
            factorycounter += 1f;
            FactoryCost *= Costincrease;

        }
    }


    public void Start()
    {
        //Factory
        FactoryCost = data.FactoryCost;
        Factoryyield = data.Factoryyield;

        //Factoryupgrade
        FactoryUpgradecost = udata.FactoryUpgradecost;
        FactoryUpgradeResearchThreshold = udata.FactoryUpgradeResearchThreshold;
        FactoryUpgradeNumber = 1f;
    }
    //Wip
    public void FactoryUpgradepress()
    {
        if (manager.counter >= FactoryUpgradecost)
        {
            if (manager.Researchcounter >= FactoryUpgradeResearchThreshold)
            {
                manager.counter -= FactoryUpgradecost;
                FactoryUpgradeNumber += 1f;
                FactoryUpgradecost *= UpgradeCostincrease;
                FactoryUpgradeResearchThreshold *= Researchincrease;
            }
        }
    }




    public void Update()
    {
        // Factory
        FactoryUI.text = factorycounter.ToString();
        FactoryUIcost.text = FactoryCost.ToString();

        FactoryIncrease = factorycounter * Factoryyield * FactoryUpgradeNumber;
        // Factoryupgrade
        FactoryUpgradeUI.text = FactoryUpgradeNumber.ToString();
        FactoryUpgradeUIcost.text = FactoryUpgradecost.ToString();
        FactoryUpgradeResearchUIText.text = FactoryUpgradeResearchThreshold.ToString();
    }
    void FixedUpdate()
    {

        manager.counter += FactoryIncrease;
    }


}


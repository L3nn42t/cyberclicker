﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonResearchAction : MonoBehaviour
{
    public Manager manager;

    public void Researchpress()
    {
        manager.Researchcounter += manager.Researchincrease;
    }
}

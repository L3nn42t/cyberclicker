﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishGame : MonoBehaviour
{
    [SerializeField]
    private Manager manager;
    private float Money;
    private float Research;
    private float Pop;

    [SerializeField]
    private GameObject EndButton;

    [SerializeField]
    private GameEndData EndData;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        Money = EndData.money;
        Research = EndData.research;
        Pop = EndData.pop;
        EndButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.counter >= Money && manager.Researchcounter >= Research && manager.Popcounter >= Pop)
        {
            EndButton.SetActive(true);
        }
    }

    public void Endpress()
    {
        SceneManager.LoadScene("EndingScene");
    }
}

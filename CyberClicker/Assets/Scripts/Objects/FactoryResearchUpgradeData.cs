﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactoryResearchUpgradeData", menuName = "FactoryResearchUpgradeData")]
public class FactoryResearchUpgradeData : ScriptableObject
{
    public float Threshold;
    public float FactoryUpgradecost;
    public float Increase;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEndData", menuName = "GameEndData")]
public class GameEndData : ScriptableObject
{
    public float money = 1000f;
    public float research = 1000f;
    public float pop = 1000f;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactoryUpgradeData", menuName = "FactoryUpgradeData")]
public class FactoryUpgradeData : ScriptableObject
{
    //Factoryupgrade
    public float FactoryUpgradecost = 100f;
    public float FactoryUpgradeNumber = 1;
    
    public float FactoryUpgradeResearchThreshold = 100;
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactoryPopData", menuName = "FactoryPopData")]
public class FactoryPopData : ScriptableObject
{
    public float Popyield;
    public float Popcost;
}

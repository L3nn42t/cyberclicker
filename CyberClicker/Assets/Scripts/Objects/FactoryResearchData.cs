﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactoryResearchData", menuName = "FactoryResearchData")]
public class FactoryResearchData : ScriptableObject
{
    public int Researchyield;

    public float Factorycost;
}

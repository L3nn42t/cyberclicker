﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BaseUpgradeData", menuName = "BaseUpgradeData")]
public class BaseUpgradeData : ScriptableObject
{
    public float Upgradecost = 10f;
    public int UpgradeResearchThreshold = 30;
}

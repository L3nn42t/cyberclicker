﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FactoryPopUpgradeData", menuName = "FactoryPopUpgradeData")]
public class FactoryPopUpgradeData : ScriptableObject
{
    public float PopUpgradecost;
    public float PopThreshold;
    public float PopIncrease;
}
